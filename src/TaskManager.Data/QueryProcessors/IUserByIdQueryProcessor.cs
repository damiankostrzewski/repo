﻿using TaskManager.Data.Entities;

namespace TaskManager.Data.QueryProcessors
{
    public interface IUserByIdQueryProcessor
    {
        User GetUserById(long userId);
    }
}
