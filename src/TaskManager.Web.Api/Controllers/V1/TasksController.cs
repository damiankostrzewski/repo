﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using TaskManager.Web.Api.InquiryProcessing;
using TaskManager.Web.Common.Routing;
using TaskManager.Web.Common.Validation;
using TaskManager.Common;
using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.Controllers.V1
{
    [ApiVersion1RoutePrefix("tasks")]
    [Authorize(Roles = Constants.RoleNames.User)]
    public class TasksController : ApiController
    {
        private readonly ITaskByIdInquiryProcessor _taskByIdInquireProcessor;

        public TasksController(ITaskByIdInquiryProcessor taskByIdInquireProcessor)
        {
            _taskByIdInquireProcessor = taskByIdInquireProcessor;
        }

        [Route("{id:long}", Name = "GetTaskRoute")]
        public Task GetTaskById(long id)
        {
            var task = _taskByIdInquireProcessor.GetTaskById(id);
            return task;
        }
    }
}