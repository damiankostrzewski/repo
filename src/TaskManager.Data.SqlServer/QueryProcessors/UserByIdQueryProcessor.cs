﻿using TaskManager.Data.Entities;
using NHibernate;
using TaskManager.Data.QueryProcessors;

namespace TaskManager.Data.SqlServer.QueryProcessors
{
    public class UserByIdQueryProcessor : IUserByIdQueryProcessor
    {
        private readonly ISession _session;

        public UserByIdQueryProcessor(ISession session)
        {
            _session = session;
        }

        public User GetUserById(long userId)
        {
            var user = _session.Get<User>(userId);
            return user;
        }
    }
}
