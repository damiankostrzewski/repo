﻿using TaskManager.Common.TypeMapping;
using TaskManager.Data.Exceptions;
using TaskManager.Data.QueryProcessors;
using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.InquiryProcessing
{
    public class StatusByIdInquiryProcessor : IStatusByIdInquiryProcessor
    {
        private readonly IAutoMapper _autoMapper;
        private readonly IStatusByIdInquiryProcessor _queryProcessor;

        public StatusByIdInquiryProcessor(IStatusByIdInquiryProcessor queryProcessor,
            IAutoMapper autoMapper)
        {
            _queryProcessor = queryProcessor;
            _autoMapper = autoMapper;
        }

        public Status GetStatusById(long statusId)
        {
            var statusEntity = _queryProcessor.GetStatusById(statusId);

            if (statusEntity == null)
            {
                throw new RootObjectNotFoundException("Status not found");
            }

            var status = _autoMapper.Map<Status>(statusEntity);
            return status;
        }
    }
}