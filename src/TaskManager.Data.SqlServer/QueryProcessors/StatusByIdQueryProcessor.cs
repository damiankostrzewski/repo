﻿using TaskManager.Data.Entities;
using NHibernate;
using TaskManager.Data.QueryProcessors;

namespace TaskManager.Data.SqlServer.QueryProcessors
{
    public class StatusByIdQueryProcessor : IStatusByIdQueryProcessor
    {
        private readonly ISession _session;

        public StatusByIdQueryProcessor(ISession session)
        {
            _session = session;
        }

        public Status GetStatusById(long taskId)
        {
            var status = _session.Get<Status>(taskId);
            return status;
        }
    }
}
