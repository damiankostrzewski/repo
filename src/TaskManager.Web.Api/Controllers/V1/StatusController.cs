﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using TaskManager.Web.Api.InquiryProcessing;
using TaskManager.Web.Common.Routing;
using TaskManager.Web.Common.Validation;
using TaskManager.Common;
using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.Controllers.V1
{
    [ApiVersion1RoutePrefix("tasks")]
    [Authorize(Roles = Constants.RoleNames.User)]
    public class StatusController : ApiController
    {
        private readonly ITaskByIdInquiryProcessor _taskByIdInquireProcessor;
        private readonly IStatusByIdInquiryProcessor _statusByIdInquireProcessor;

        public StatusController(ITaskByIdInquiryProcessor taskByIdInquireProcessor, 
            IStatusByIdInquiryProcessor statusByIdInquireProcessor)
        {
            _statusByIdInquireProcessor = statusByIdInquireProcessor;
            _taskByIdInquireProcessor = taskByIdInquireProcessor;
        }

        [Route("SetNotStarted/{id:long}", Name = "SetNotStarted")]
        public void SetNotStarted(long id)
        {
            var status = _statusByIdInquireProcessor.GetStatusById(1);
            var task = _taskByIdInquireProcessor.GetTaskById(id);

            task.Status = status;
        }

        [Route("SetInProgress/{id:long}", Name = "SetInProgress")]
        public void SetInProgress(long id)
        {
            var status = _statusByIdInquireProcessor.GetStatusById(2);
            var task = _taskByIdInquireProcessor.GetTaskById(id);

            task.Status = status;
        }

        [Route("SetCompleted/{id:long}", Name = "SetCompleted")]
        public void SetCompleted(long id)
        {
            var status = _statusByIdInquireProcessor.GetStatusById(3);
            var task = _taskByIdInquireProcessor.GetTaskById(id);

            task.Status = status;
        }
    }
}
