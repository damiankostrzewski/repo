﻿using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.InquiryProcessing
{
    public interface IStatusByIdInquiryProcessor
    {
        Status GetStatusById(long statusId);
    }
}
