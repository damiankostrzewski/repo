﻿using TaskManager.Data.Entities;

namespace TaskManager.Data.QueryProcessors
{
    public interface IStatusByIdQueryProcessor
    {
        Status GetStatusById(long statusId);
    }
}
