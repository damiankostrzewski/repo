﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using TaskManager.Web.Api.InquiryProcessing;
using TaskManager.Web.Common.Routing;
using TaskManager.Web.Common.Validation;
using TaskManager.Common;
using TaskManager.Web.Api.Models;

namespace TaskManager.Web.Api.Controllers.V1
{
    [ApiVersion1RoutePrefix("users")]
    [Authorize(Roles = Constants.RoleNames.User)]
    public class UsersController : ApiController
    {
        private readonly IUserByIdInquiryProcessor _userByIdInquireProcessor;

        public UsersController(IUserByIdInquiryProcessor userByIdInquireProcessor)
        {
            _userByIdInquireProcessor = userByIdInquireProcessor;
        }

        [Route("{id:long}", Name = "GetUserRoute")]
        public User GetUserById(long id)
        {
            var user = _userByIdInquireProcessor.GetUserById(id);
            return user;
        }
    }
}
